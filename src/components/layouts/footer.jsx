import React from "react";
import { Layout, Col, Row } from "antd";
import "../../assets/styles/footer.css";
import DAVivienda from "../../assets/img/Group2.png";
import Epayco from "../../assets/img/Group-1.png";

const { Footer } = Layout;

const FooterLayout = () => {
  return (
    <Footer className="footer-home">
      <Row>
        <Col span={12} className="footer-text">
          <span>ePayco © 2011 - 2019 todos los derechos reservados.</span>
        </Col>
        <Col span={12} className="footer-logo">
          <img src={DAVivienda} alt="" />
          <img src={Epayco} alt="" />
        </Col>
      </Row>
    </Footer>
  );
};

export default FooterLayout;
