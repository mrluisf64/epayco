import React from "react";
import "../../assets/styles/sidebar.css";
import { Layout, Menu } from "antd";
import { HomeOutlined, TeamOutlined } from "@ant-design/icons";
import logoPNG from "../../assets/img/logo.png";

const { Sider } = Layout;

const Sidebar = () => {
  return (
    <Sider
      breakpoint="lg"
      collapsedWidth="0"
      style={{
        backgroundColor: "#3A3A3A",
      }}
    >
      <div>
        <img src={logoPNG} alt="" className="logo-img" />
        <div className="title-sidebar">
          <h4>ALEXANDER CEBA...</h4>
        </div>
      </div>
      <Menu
        theme="dark"
        defaultSelectedKeys={["1"]}
        mode="inline"
        style={{ backgroundColor: "#3A3A3A" }}
      >
        <Menu.Item key="1" icon={<HomeOutlined />}>
          Dashboard
        </Menu.Item>
        <Menu.Item key="2" icon={<TeamOutlined />}>
          Clientes
        </Menu.Item>
      </Menu>
    </Sider>
  );
};

export default Sidebar;
