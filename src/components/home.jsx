import React from "react";
import Sidebar from "./layouts/sidebar";
import { Layout } from "antd";
import FooterLayout from "./layouts/footer";
import Register from "./register";

const { Header } = Layout;

const Home = () => {
  return (
    <Layout style={{ minHeight: "100vh" }}>
      <Sidebar />
      <Layout className="site-layout">
        <Header
          className="site-layout-sub-header-background"
          style={{
            padding: 0,
            backgroundColor: "#fff",
            borderBottom: "#EAEAEA solid 3px",
          }}
        />
        <Register />
        <FooterLayout />
      </Layout>
    </Layout>
  );
};

export default Home;
