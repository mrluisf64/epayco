import React from "react";
import { Layout } from "antd";
import FormLayout from "./form";

const { Content } = Layout;

const Register = () => {
  return (
    <Content style={{ margin: "24px 16px 0", overflow: "initial" }}>
      <div className="site-layout-background">
        <FormLayout />
      </div>
    </Content>
  );
};

export default Register;
