import React from "react";
import { Modal, Button } from "antd";
import failedImg from "../../../assets/img/Group 3.png";
import "../../../assets/styles/modal.css";

const ModalFailed = (props) => {
  return (
    <Modal
      visible={props.visible}
      onCancel={props.handleFinally}
      footer={[
        <Button key="submit" type="primary" block onClick={props.handleFinally}>
          Regresar
        </Button>,
      ]}
    >
      <div className="content-modal">
        <img src={failedImg} alt="" />
        <p>El formulario contiene errores</p>
      </div>
    </Modal>
  );
};

export default ModalFailed;
