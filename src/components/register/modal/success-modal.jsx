import React from "react";
import { Modal, Button } from "antd";
import successImg from "../../../assets/img/Group 3-1.png";
import "../../../assets/styles/modal.css";

const ModalSuccess = (props) => {
  return (
    <Modal
      visible={props.visible}
      onCancel={props.handleFinally}
      footer={[
        <Button key="submit" type="primary" block onClick={props.handleFinally}>
          Finalizar
        </Button>,
      ]}
    >
      <div className="content-modal">
        <img src={successImg} alt="" />
        <p>Formulario enviado con éxito</p>
      </div>
    </Modal>
  );
};

export default ModalSuccess;
