import React, { useEffect, useState } from "react";
import { Form, Input, Button, Col, Row, Select } from "antd";
import "../../../assets/styles/form.css";
import RegisterService from "../../../services/registerService";
import ModalSuccess from "../modal/success-modal";
import ModalFailed from "../modal/failed-modal";

const { Option } = Select;

const tailLayout = {
  wrapperCol: { span: 6 },
};

const FormLayout = () => {
  const [countries, setCountries] = useState([]);
  const [modalSuccess, setModalSuccess] = useState(false);
  const [modalFailed, setModalFailed] = useState(false);

  const handleFinally = () => {
    setModalSuccess(false);
  };

  const handleFinallyFailed = () => {
    setModalFailed(false);
  };

  const onFinish = (values) => {
    setModalSuccess(true);
  };

  const onFinishFailed = (errorInfo) => {
    setModalFailed(true);
  };

  useEffect(() => {
    RegisterService.getCountries().then((response) => {
      const { data } = response;
      setCountries(data.filter((val, index) => index <= 19));
    });
  }, []);

  return (
    <>
      <div className="title-form">
        <h4>Información del formulario</h4>
      </div>
      <hr />
      <div className="text-form">
        <p>
          Ingrese el titulo y la descripción que visualizarán los usuarios
          durante el proceso de pago
        </p>
      </div>
      <Form
        name="basic"
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
      >
        <Row style={{ padding: 10 }}>
          <Col span={12} style={{ padding: 10 }}>
            <Form.Item
              label="Nombre"
              name="firstName"
              rules={[
                { required: true, message: "Por favor ingrese su nombre" },
              ]}
            >
              <Input />
            </Form.Item>
          </Col>

          <Col span={12} style={{ padding: 10 }}>
            <Form.Item
              label="Apellidos"
              name="lastName"
              rules={[
                { required: true, message: "Por favor ingrese su apellido" },
              ]}
            >
              <Input />
            </Form.Item>
          </Col>
        </Row>

        <Row style={{ padding: 10 }}>
          <Col span={12} style={{ padding: 10 }}>
            <Form.Item
              label="Seleccione un pais"
              name="country"
              rules={[{ required: true, message: "Por favor ingrese su pais" }]}
            >
              <Select>
                {countries &&
                  countries.map((country) => {
                    return (
                      <Option value={country.alpha2Code}>{country.name}</Option>
                    );
                  })}
              </Select>
            </Form.Item>
          </Col>

          <Col span={12} style={{ padding: 10 }}>
            <Form.Item
              label="Número de documento"
              name="cid"
              rules={[
                {
                  required: true,
                  message: "Por favor ingrese su numero de documento",
                },
              ]}
            >
              <Input />
            </Form.Item>
          </Col>
        </Row>

        <Form.Item {...tailLayout}>
          <Button type="primary" htmlType="submit">
            Enviar
          </Button>
          <Button>Cancelar</Button>
        </Form.Item>
      </Form>
      <ModalSuccess visible={modalSuccess} handleFinally={handleFinally} />
      <ModalFailed visible={modalFailed} handleFinally={handleFinallyFailed} />
    </>
  );
};

export default FormLayout;
