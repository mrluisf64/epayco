import axios from "axios";

class RegisterService {
  getCountries() {
    return axios.get("https://restcountries.eu/rest/v2/all");
  }
}

export default new RegisterService();
